<?php

use App\Http\Controllers\AdminPaysController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/contactus', function(){
    return view('formContact');
});
// Route::get('/test/{nom}', function($nom){
// dd($nom);
// });
Route::get('/pays',[AdminPaysController::class, "index"]);
Route::put('/pays',[AdminPaysController::class, "modifPays"]);
