@extends("template")
@section('titre')
Pays
@endsection

@section('content')
<h1>Les pays</h1>

<table class="table">
    <thead>
        <th>id</th>
        <th>Nom</th>
        <th>Région</th>
        <th>Drapeau</th>
        <th>Action</th>
    </thead>
    <tbody>
        @foreach ($desPays as $unPays)


        <tr>
            <td>{{$unPays->id}}</td>
            <td>{{$unPays->nom}}</td>
            <td>{{$unPays->region}}</td>
            <td>{{$unPays->drapeau}}</td>
            <td><a href="pays/{{$unPays->id}}/modifPays" class="btn btn-primary">Modifier</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
