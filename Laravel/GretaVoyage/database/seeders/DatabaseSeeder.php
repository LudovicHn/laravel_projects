<?php

namespace Database\Seeders;

use App\Models\Pays;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $admin=Role::create(["nom"=>"admin"]);
        $client=Role::create(["nom"=>"client"]);

        $moi=User::create([
            "name"=>"LHanon",
            "email"=>"ludovic.hanon@gmail.com",
            "password"=>bcrypt("ludovic.hanon@gmail.com1"),
            "role_id"=>$admin->id]);
            
        $lesPays=Pays::factory(10)->create();
    }
}
