<?php

namespace App\Http\Controllers;

use App\Models\Pays;
use Illuminate\Http\Request;

class AdminPaysController extends Controller
{
    public function index(){
        $lesPays=Pays::all();
        return view("admin.pays.index", ["desPays"=>$lesPays]);
    }
    public function modifPays(){
        return view("admin.pays.modifPays");
    }
}
