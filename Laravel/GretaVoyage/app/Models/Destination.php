<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;
    protected $guarded=["id"]; // ce que l'on veut protéger

    public function pays(){
        return $this->belongsTo(Pays::class); // La destination appartient au pays
    }
    public function commentaires(){
        return $this->hasMany(Commentaire::class);
    }
    public function lignes(){
        return $this->belongsToMany(Commande::class, "lignes")->using(Ligne::class)->with(["quantite"])->withTimestamps();
    }
    public function notes(){
        return $this->belongsToMany(User::class, "notes")->using(Note::class)->with(["score"])->withTimestamps();
    }
}
