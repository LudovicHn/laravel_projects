<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    use HasFactory;
    protected $guarded=["id"]; // ce que l'on veut protéger

    public function destinations(){
        return $this->hasMany(Destination::class); // Le pays a plusieurs destinations
    }
}
